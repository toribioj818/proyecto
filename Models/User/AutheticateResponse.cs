using System;
using System.Text.Json.Serialization;
using Local.Entities;

namespace Local.Models.User
{
    public class AutheticateResponse
    {
        public Guid id_user { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string JwtToken { get; set; }

        [JsonIgnore]
        public string RefreshToken { get; set; }

        public AutheticateResponse(Users user, string jwtToken, string refreshToken)
        {
            id_user = user.id_user;
            name = user.name;
            last_name = user.last_name;
            username = user.username;
            email = user.email;
            phone_number = user.phone_number;
            JwtToken = jwtToken;
            RefreshToken = refreshToken;
        }
    }
}