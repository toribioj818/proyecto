using System.ComponentModel.DataAnnotations;

namespace Local.Models
{
    public class Register
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string last_name { get; set; }

        [Required]
        public string username { get; set; }

        [Required]
        public string email { get; set; }

        [Required]
        public string phone_number { get; set; }

        public byte[] cover { get; set; }

        [Required]
        public string password { get; set; }
    }
}