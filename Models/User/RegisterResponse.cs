using Local.Entities;

namespace Local.Models.User
{
    public class RegisterResponse
    {
        public string name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string JwtToken { get; set; }

        public RegisterResponse(Users user, string jwtToken)
        {
            name = user.name;
            last_name = user.last_name;
            username = user.username;
            email = user.email;
            phone_number = user.phone_number;
            JwtToken = jwtToken;
        }
    }
}