
namespace Local.Models.User
{
    public class Update
    {
        public string name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string password { get; set; }
    }
}