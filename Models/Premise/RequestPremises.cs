using System.ComponentModel.DataAnnotations;

namespace Local.Models.Premise
{
    public class RequestPremises
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string owner { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        public string latitude { get; set; }

        [Required]
        public string longitud { get; set; }
        public byte[] cover { get; set; }

        public string id_user { get; set; }
    }
}