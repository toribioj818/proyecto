using System;
using Local.Entities;

namespace Local.Models.Premise
{
    public class ResponsePremises
    {
        public Guid id_premises { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public string description { get; set; }
        public string latitude { get; set; }
        public string longitud { get; set; }
        public byte[] cover { get; set; }
        public string id_user { get; set; }

        public ResponsePremises(Premises premise)
        {
            id_premises = premise.id_premises;
            name = premise.name;
            owner = premise.owner;
            description = premise.description;
            latitude = premise.latitude;
            longitud = premise.longitud;
            cover = premise.cover;
            id_user = premise.id_user.ToString();
        }
    }
}