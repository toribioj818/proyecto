using System;
using AutoMapper;
using Local.Entities;
using Local.Helpers;
using Local.Models;
using Local.Models.User;
using Local.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Local.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private IUserServices userService;
        private IMapper mapper;
        private readonly AppSettings appSettings;
        
        public UserController(IUserServices _useService, IMapper _mapper, IOptions<AppSettings> _appSettings)
        {
            userService = _useService;
            mapper = _mapper;
            appSettings = _appSettings.Value;
        } 
        
        // Endpoints
        [AllowAnonymous]
        [HttpPost("Registro")]
        public IActionResult Registro([FromBody]Register model)
        {
            var user = mapper.Map<Users>(model);
            try
            {
                var response = userService.CreateUser(user, model.password);
                return Ok(response);
            }
            catch(AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [AllowAnonymous]
        [HttpPost("Autenticacion")]
        public IActionResult Authenticate([FromBody] Login model)
        {
            var response = userService.Authenticate(model, ipAddress(), model.password);
            if(response == null)
                return BadRequest(new { message = "Correo o Clave Incorrectas" });

            setTokeCookie(response.RefreshToken);

            return Ok(response);
        }

        [HttpPut("Actualizar/{id}")]
        public IActionResult UpdateUser(Guid id, [FromBody]Update model)
        {
            var user = mapper.Map<Users>(model);
            user.id_user = id;

            try
            {
                userService.Update(user, model.password);
                return Ok();
            }
            catch(AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("Refresh-Token")]
        public IActionResult RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var response = userService.RefreshToken(refreshToken, ipAddress());

            if(response == null)
                return Unauthorized(new { message = "Token Invalido" });
            
            setTokeCookie(response.RefreshToken);
            return Ok(response);
        }

        [HttpPost("Revoke-Token")]
        public IActionResult RevokeToken([FromBody] RevokeTokenRequest model)
        {
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if(string.IsNullOrEmpty(token))
                return BadRequest(new { message = "El toke es requerido." });

            var response = userService.RevokeToken(token, ipAddress());

            if(!response)
                return NotFound(new { message = "Toke no encontrado" });

            return Ok(new { message = "El token ha sido revocado" });
        }

        // Private Methods
        private void setTokeCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        private string ipAddress()
        {
            if(Request.Headers.ContainsKey("X-Forwarded-for"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}