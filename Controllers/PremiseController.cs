using System;
using System.Linq;
using AutoMapper;
using Local.Entities;
using Local.Helpers;
using Local.Models.Premise;
using Local.Services.Premise;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Local.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class PremiseController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IPremisesServices premisesServices;
        private readonly DataContext dataContext;
        public PremiseController(IMapper _mapper, IPremisesServices _premisesServices, DataContext _dataContext)
        {
            mapper = _mapper;
            premisesServices = _premisesServices;
            dataContext = _dataContext;
        }

        [HttpPost("Registro")]
        public IActionResult Register([FromBody] RequestPremises model)
        {
            Guid id;
            var premises = mapper.Map<Premises>(model);
            var claims = User.Claims.ToList();

            var getId = claims.FirstOrDefault(x => x.Type == "unique_name");
            id = new Guid(getId.Value);
            Console.WriteLine(id);

            var user = dataContext.Users.Where(x => x.id_user == id);

            try
            {
                var response = premisesServices.Create(premises, id);
                return Ok(response);
            }
            catch(AppException ex)
            {
                return BadRequest(new { messager = ex.Message });
            }
        }
    }
}