using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Local.Entities
{
    public class Users
    {
        public Guid id_user { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public byte[] cover { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        [JsonIgnore]
        public List<RefreshToken> RefreshTokens { get; set; }
        public ICollection<Premises> Premises { get; set; }

    }
}