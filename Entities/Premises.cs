using System;
using System.ComponentModel.DataAnnotations;

namespace Local.Entities
{
    public class Premises
    {
        public Guid id_premises { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public string description { get; set; }
        public string latitude { get; set; }
        public string longitud { get; set; }
        public byte[] cover { get; set; }
        public Guid id_user { get; set; }
        public Users Users { get; set; }
    }
}