using System;
using Local.Entities;
using Local.Models.Premise;

namespace Local.Services.Premise
{
    public interface IPremisesServices
    {
         ResponsePremises Create(Premises premises, Guid id);
    }
}