using System;
using System.Linq;
using Local.Entities;
using Local.Helpers;
using Local.Models.Premise;

namespace Local.Services.Premise
{
    public class PremisesServices : IPremisesServices
    {
        private readonly DataContext dataContext;
        public PremisesServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }
        
        public ResponsePremises Create(Premises premises, Guid id)
        {
            if(dataContext.Premises.Any(x => x.name == premises.name))
                throw new AppException("El nombre " + premises.name + " ya esta registrado.");
            
            premises.id_premises = Guid.NewGuid();
            premises.id_user = id;

            dataContext.Premises.Add(premises);
            dataContext.SaveChanges();

            return new ResponsePremises(premises);
        }
    }
}