using Local.Entities;
using Local.Models;
using Local.Models.User;

namespace Local.Services
{
    public interface IUserServices
    {
        RegisterResponse CreateUser(Users user, string password);
        AutheticateResponse Authenticate(Login model, string ipAddress, string password);
        AutheticateResponse RefreshToken(string token, string ipAddress);
        bool RevokeToken(string token, string ipAddress);
        void Update(Users userParam, string password = null);
    }
}