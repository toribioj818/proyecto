using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Local.Entities;
using Local.Helpers;
using Local.Models;
using Local.Models.User;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Local.Services.User
{
    public class UserServices : IUserServices
    {
        private DataContext dataContext;
        private readonly AppSettings appSettings;
        public UserServices(DataContext _dataContext, IOptions<AppSettings> _appSettings)
        {
            dataContext = _dataContext;
            appSettings = _appSettings.Value;
        }

        // Create
        public RegisterResponse CreateUser(Users user, string password)
        {
            if(string.IsNullOrWhiteSpace(password))
                throw new AppException("La clave es requerida.");

            if(dataContext.Users.Any(x => x.email == user.email))
                throw new AppException("El Correo ingresado ya existe.");

            if(dataContext.Users.Any(x => x.username == user.username))
                throw new AppException("El nombre de usuario ingresado ya existe.");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            // Token
            var jwtToken = generateJwtToken(user);

            user.id_user = Guid.NewGuid();
            dataContext.Users.Add(user);
            dataContext.SaveChanges();

            return new RegisterResponse(user, jwtToken);
        }


        // Login
        public AutheticateResponse Authenticate(Login model, string ipAddress, string password)
        {
            var user = dataContext.Users.SingleOrDefault(x => x.email == model.email);
            if(user == null) return null;

            if(!VerifyPassword(password, user.PasswordHash, user.PasswordSalt))
                return null;

            var jwtToken = generateJwtToken(user);
            var refreshToken = generateRefreshToken(ipAddress);

            user.RefreshTokens.Add(refreshToken);
            dataContext.Update(user);
            dataContext.SaveChanges();

            return new AutheticateResponse(user, jwtToken, refreshToken.Token);
        }

        // Update
        public void Update(Users userParam, string password = null)
        {
            var user = dataContext.Users.Find(userParam.id_user);

            if(user == null)
                throw new AppException("Usuario no encontrado.");
            
            if(!string.IsNullOrWhiteSpace(userParam.username) && userParam.username != user.username)
            {
                if(dataContext.Users.Any(x => x.username == userParam.username))
                    throw new AppException("El nombre usuario " + userParam.username + " ya esta en uso.");

                if(dataContext.Users.Any(x => x.email == userParam.email))
                    throw new AppException("El correo " + userParam.email + " ya esta en uso.");

                if(!string.IsNullOrWhiteSpace(userParam.username))
                    user.username = userParam.username;

                if(!string.IsNullOrWhiteSpace(userParam.email))
                    user.email = userParam.email;
            }

            if(!string.IsNullOrWhiteSpace(userParam.name))
                user.name = userParam.name;

            if(!string.IsNullOrWhiteSpace(userParam.last_name))
                user.last_name = userParam.last_name;

            if(!string.IsNullOrWhiteSpace(userParam.phone_number))
                user.phone_number = userParam.phone_number;

            if(userParam.cover == null)
                user.cover = userParam.cover;

            if(!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;

                CreatePasswordHash(password, out passwordHash, out passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            dataContext.Users.Update(user);
            dataContext.SaveChanges();

        }

        public AutheticateResponse RefreshToken(string token, string ipAddress)
        {
            var user = dataContext.Users.SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            if(user == null) return null;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            if(!refreshToken.IsActive) return null;

            var newRefreshToken = generateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokeByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;
            user.RefreshTokens.Add(newRefreshToken);

            dataContext.Update(user);
            dataContext.SaveChanges();

            // Generate new Token
            var jwtToken = generateJwtToken(user);
            return new AutheticateResponse(user, jwtToken, newRefreshToken.Token);
        }

        public bool RevokeToken(string token, string ipAddress)
        {
            var user = dataContext.Users.SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            if(user == null) return false;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            if(!refreshToken.IsActive) return false;

            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokeByIp = ipAddress;
            dataContext.Update(user);
            dataContext.SaveChanges();

            return true;
        }

    
        // Private User Methods
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if(password == null) throw new ArgumentException("password");

            if(string.IsNullOrWhiteSpace(password)) throw new ArgumentException("El valor de la clave no puede ser vacio.", "password");

            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPassword(string password, byte[] storedHash, byte[] storedSalt)
        {
            if(password == null) throw new ArgumentException("password");
            if(string.IsNullOrWhiteSpace(password)) throw new ArgumentException("El valor de la clave no puede ser vacio.", "password");
            if(storedHash.Length != 64) throw new ArgumentException("Longitud no válida de hash de contraseña (se esperan 64 bytes). ", "passwordHash");
            if(storedSalt.Length != 128) throw new ArgumentException("Longitud no válida de la contraseña salt (se esperan 128 bytes).", "passwordSalt");

            using(var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for(int i = 0; i < computeHash.Length; i++)
                {
                    if(computeHash[i] != storedHash[i]) return false;
                }
            }
            return true;
        }

        private string generateJwtToken(Users user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.id_user.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken generateRefreshToken(string ipAddress)
        {
            using(var rngCryptoServiceProvier = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvier.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreateByIp = ipAddress
                };
            }
        }
    }
}