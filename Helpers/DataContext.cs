using Local.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Local.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public DataContext(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
        }

        // Tables Settings
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Primary key
            modelBuilder.Entity<Users>().HasKey(b => b.id_user);
            modelBuilder.Entity<Premises>().HasKey(b => b.id_premises);

            // Relationship
            modelBuilder.Entity<Premises>()
                .HasOne(x => x.Users)
                .WithMany(x => x.Premises)
                .HasForeignKey(x => x.id_user);
        }

        // Tables
        public DbSet<Users> Users { get; set; }
        public DbSet<Premises> Premises { get; set; }
        
    }
}